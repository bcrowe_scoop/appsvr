import passport from 'passport'
import config from './config'
import { Strategy as LocalStrategy } from 'passport-local'
import passportJWT from 'passport-jwt'
import {Authenticate} from './auth'
import log from './logger'
log.info('Server', 'Setting up passport for authentication.')

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, cb) {
        // Custom Authentication Promise using auth service
        Authenticate( {email, password }).then(auth_results => {
            if(!auth_results.status){
                return cb(null, auth_results.status, { message: auth_results.message } )
            }
            return cb(null, auth_results.user, { message: auth_results.message })
        }).catch(err => cb(err))
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.jwt_secret
    },
    function(jwtPayload, cb) {
        //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
        return cb(null, jwtPayload)
    }
));