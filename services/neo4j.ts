import neo4j from 'neo4j-driver'
import config from './config'
const driver = neo4j.driver(`neo4j://${config.database.host}:${config.database.port}`, null, { disableLosslessIntegers: true })

export default driver