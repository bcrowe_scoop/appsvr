export default {
    info(namespace: String, message: String){
        let d = new Date().toISOString()
        console.info(`[${d}] [${namespace}]: ${message}`)
    },
    error(namespace: String, message: String){
        let d = new Date().toISOString()
        console.error(`[${d}] [${namespace}]: ${message}`)
    },
    warning(namespace: String, message: String){
        let d = new Date().toISOString()
        console.warn(`[${d}] [${namespace}]: ${message}`)
    },
    debug(namespace: String, message: String){
        let d = new Date().toISOString()
        console.debug(`[${d}] [${namespace}]: ${message}`)
    },
}