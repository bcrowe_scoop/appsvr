import config from './config'
const Minio = require('minio');
const minioClient = new Minio.Client({
    endPoint: config.minio.endPoint,
    port: config.minio.port,
    useSSL: config.minio.useSSL,
    accessKey: config.minio.accessKey,
    secretKey: config.minio.secretKey
});
export default {
    getObject(object, bucket, local_path) {
        return new Promise((resolve, reject) => {
            minioClient.fGetObject(bucket, object, local_path, function (err) {
                if (err) {
                    return reject(err)
                }
                console.log(`[MINIO] Object: ${object} downloaded from storage at bucket: ${bucket}, to: ${local_path}`)
                return resolve(local_path)
            })
        })
    },
    putObject(object, bucket, local_path, meta_data) {
        return new Promise((resolve, reject) => {
            minioClient.fPutObject(bucket, object, local_path, meta_data, function (err, etag) {
                if (err) {
                    return reject(err)
                }
                console.log(`[MINIO] Image uploaded successfully ${object}`)
                return resolve(object)
            })
        });

    },
    removeObject(object, bucket) {
        return new Promise((resolve, reject) => {
            minioClient.removeObject(bucket, object, function (err) {
                if (err) {
                    return reject(err)
                }
                console.log(`[MINIO] Removed object at bucket: ${bucket}, object: ${object}`)
                return resolve(object)
            })
        });
    }
}