// Graphql
import jwt from 'jsonwebtoken'
import config from '../config'
import { makeAugmentedSchema } from 'neo4j-graphql-js';
import { ApolloServer, gql, makeExecutableSchema } from 'apollo-server-express'
import typeDefs from '../../models/typedefs'
import driver from '../neo4j'
import { resolvers } from './resolvers'
function verifyToken(auth_header: String){
    if(auth_header && typeof auth_header != 'undefined'){
        var parts = auth_header.split(' ');
        if (parts.length === 2) {
            if (parts[0] == 'Bearer') {
                try {
                    var user = jwt.verify(parts[1], config.jwt_secret)
                    return user
                } catch (error) {
                    console.log(error)
                    return false
                }
            } else {
                return false
            }
        }
    }else {
        return false
    }
    
}
const schema = makeAugmentedSchema({
    typeDefs,
    resolvers
})

export const apolloserver = new ApolloServer({ 
    schema, 
    context({ req }) {
        var user = verifyToken(req.headers.authorization)
        return { 
            req, 
            driver, 
            user,
            cypherParams: {
                user_uuid: user.uuid
            }
        }
    }
})
