import { neo4jgraphql } from "neo4j-graphql-js";
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
import log from '../logger'
import event_bus from '../event'
import { v4 as uuidv4 } from 'uuid'
import { ds } from '../datastore/ds'
export const resolvers = {
    
    Mutation:{
        async SetCategoryToVideo(object, params, ctx, resolveInfo){
            const query1 = `
                MATCH (v:Video { uuid: $video_uuid} )-[r:IN_CATEGORY]->(:Category) DELETE r
            `
            const query2 = `
                MATCH (v2:Video {uuid: $video_uuid}), (c:Category {name: $category_name})
                MERGE (v2)-[r2:IN_CATEGORY]->(c) RETURN v2{.*}
            `
            const data = {
                video_uuid: params.video_uuid,
                category_name: params.category_name
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                await d.customquery(query1, data)
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async WatchVideo(object, params, ctx, resolveInfo){
            const query1 = `
                MATCH (:Profile { uuid: $profile_uuid } )-[watched:WATCHED]->(:Video { uuid: $video_uuid } ) DELETE watched
            `
            const query2 = `
                MATCH (profile:Profile { uuid: $profile_uuid } ), (video:Video { uuid: $video_uuid } )
                MERGE (profile)-[:WATCHED { when: $when}]->(video) RETURN video{.*}
            `
            const data = {
                video_uuid: params.video_uuid,
                profile_uuid: params.profile_uuid,
                when: DateTime.fromStandardDate(new Date())
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                await d.customquery(query1, data)
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async ReactToVideo(object, params, ctx, resolveInfo){
            const query1 = `
                MATCH ( video:Video { uuid: $video_uuid} )<-[likes:REACTED]-(:Profile {uuid: $profile_uuid}) DELETE likes
            `
            const query2 = `
                MATCH ( video:Video { uuid: $video_uuid} ), ( profile:Profile { uuid: $profile_uuid } )
                MERGE (video)<-[:REACTED {status: $like_status}]-(profile) RETURN video{.*}
            `
            const data = {
                video_uuid: params.video_uuid,
                profile_uuid: params.profile_uuid,
                like_status: params.like_status
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                await d.customquery(query1, data)
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async ReactToComment(object, params, ctx, resolveInfo){
            const query1 = `
                MATCH ( comment:Comment { uuid: $comment_uuid} )<-[likes:REACTED]-(:Profile {uuid: $profile_uuid}) DELETE likes
            `
            const query2 = `
                MATCH ( comment:Comment { uuid: $comment_uuid} ), ( profile:Profile { uuid: $profile_uuid } )
                MERGE (comment)<-[:REACTED {status: $like_status}]-(profile) RETURN comment{.*}
            `
            const data = {
                comment_uuid: params.comment_uuid,
                profile_uuid: params.profile_uuid,
                like_status: params.like_status
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                await d.customquery(query1, data)
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async CommentOnVideo(object, params, ctx, resolveInfo){
            const query2 = `
                MATCH (p:Profile {uuid: $profile_uuid }), (v:Video { uuid: $video_uuid })
                CREATE(c:Comment {uuid: $new_uuid, content: $content, created: $created})
                CREATE (p)<-[:CREATED_BY]-(c)-[:BELONGS_TO]->(v)
                RETURN v{.*}
            `
            const data = {
                video_uuid: params.video_uuid,
                profile_uuid: params.profile_uuid,
                content: params.content,
                new_uuid: uuidv4(),
                created: DateTime.fromStandardDate(new Date())
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async ReplyToComment(object, params, ctx, resolveInfo){
            const query2 = `
                MATCH (p:Profile {uuid: $profile_uuid }), (c:Comment { uuid: $comment_uuid })
                CREATE(r:Reply {uuid: $new_uuid, content: $content})
                CREATE (p)<-[:CREATED_BY]-(r)-[:BELONGS_TO]->(c)
                RETURN c{.*}
            `
            const data = {
                comment_uuid: params.comment_uuid,
                profile_uuid: params.profile_uuid,
                content: params.content,
                new_uuid: uuidv4(),
                created: DateTime.fromStandardDate(new Date())
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                var results2 = await d.customquery(query2, data)
            }
            finally {
                
            }
            return results2
        },
        async DeleteCommentFromVideo(object, params, ctx, resolveInfo){
            const query2 = `
                MATCH (v:Video {uuid: $video_uuid}), (c:Comment {uuid: $comment_uuid})
                WITH c, c.uuid as uuid
                DETACH DELETE c
                RETURN {uuid: uuid}
            `
            const data = {
                comment_uuid: params.comment_uuid,
                video_uuid: params.video_uuid,
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                var results2 = await d.customquery(query2, data)
                console.log(results2)
            }
            finally {
                
            }
            return results2
        },
        async DeleteReplyFromComment(object, params, ctx, resolveInfo){
            const query2 = `
                MATCH (r:Reply {uuid: $reply_uuid})
                WITH r, r.uuid as uuid
                DETACH DELETE r
                RETURN {uuid: uuid}
            `
            const data = {
                comment_uuid: params.comment_uuid,
                reply_uuid: params.reply_uuid,
            }
            try {
                const d = new ds(ctx.driver, 'Video')
                var results2 = await d.customquery(query2, data)
                console.log(results2)
            }
            finally {
                
            }
            return results2
        },
        async DeleteVideo(object, params, ctx, resolveInfo){
            try {
                const VideoStore = new ds(ctx.driver, 'Video')
                var video = await VideoStore.selectOne(params.uuid)
                var results = await neo4jgraphql(object, params, ctx, resolveInfo)
                
            }
            finally{

                log.info('Event', `Emitting event: '/video/deleted' for video: ${params.uuid} `)
                event_bus.emit('/video/deleted', {user_uuid: ctx.user.uuid,  video })
                console.log(ctx)
            }
            return results
            
        },
        async AddHashtagToVideo(object, params, ctx, resolveInfo){
            const HashtagStore = new ds(ctx.driver, 'Hashtag')
            const hashtag_name = String(params.hashtag_name).toLowerCase()
            const query = `
                MATCH (v:Video {uuid: $uuid})
                MERGE (h:Hashtag {name: $name})
                MERGE (h)<-[r:TAGGED_WITH]-(v)
                RETURN h{.*}
            `
            var Hashtag = await HashtagStore.customquery(query, { name: hashtag_name, uuid: params.video_uuid})
            return Hashtag
        },
        
    }
}