var amqp = require('amqplib/callback_api');
import { Connection } from 'amqplib';
import config from './config'
import log from './logger'
const ns: String = 'RabbitMQ'
// if the connection is closed or fails to be established at all, we will reconnect
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];
function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

export function publish(exchange, routingKey, data) {
    log.info(ns, `[AMQP] Publishing job to Exchange: ${exchange}, Worker queue: ${routingKey}`)
    // Parse data into Buffer from JSON string
    try {
        var content = Buffer.from(JSON.stringify(data))
    } catch (error) {
        log.error(ns, `Publish error: ${error.message}`)
    }

    try {
        pubChannel.publish(exchange, routingKey, content, { persistent: true },
            function (err, ok) {
                if (err) {
                    log.error(ns, `Publish error:, ${err.message}`);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }
            });
    } catch (e) {
        console.error(ns, `[AMQP] publish error:, ${e.message}`);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}

export function start(): Promise<Connection> {
    return new Promise((resolve, reject) => {
        if (amqpConn) {
            resolve(amqpConn)
        } else {
            var url = `amqp://${config.rabbitmq.host}` + "?heartbeat=60"

            log.info(ns, `Connecting to RabbitMQ at ${url}`)
            amqp.connect(url, function (err, conn) {
                if (err) {
                    log.error(ns, `Connection error:, ${err.message}`);
                    return setTimeout(start, 1000);
                }
                conn.on("error", function (err) {
                    if (err.message !== "Connection closing") {
                        log.error(ns, `Connection error", ${err.message}`);
                    }
                });
                conn.on("close", function () {
                    log.error(ns, "Reconnecting");
                    return setTimeout(start, 1000);
                });

                log.info(ns, "Connected");
                amqpConn = conn;

                // Start Publisher
                amqpConn.createConfirmChannel(function (err, ch) {
                    if (closeOnErr(err)) return;
                    ch.on("error", function (err) {
                        log.error(ns, `Channel error:, ${err.message}`);
                    });
                    ch.on("close", function () {
                        log.info(ns, `Channel closed`);
                    });

                    pubChannel = ch;
                    resolve(amqpConn)
                    while (true) {
                        var m = offlinePubQueue.shift();
                        if (!m) break;
                        publish(m[0], m[1], m[2]);
                    }
                });

            });
        }
    });
    
}
