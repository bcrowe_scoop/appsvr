import { process_query_array } from './util'
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
import { v4 as uuidv4 } from 'uuid'

export class Video {
    label: String
    driver: any
    constructor(driver: any) {
        this.driver = driver
        this.label = 'Video'
    }

    async select() {
        const ses = this.driver.session()
        try {
            const result = await ses.run(
                `MATCH (objects:${this.label}) RETURN objects`
            )
            return process_query_array(result.records)
        } finally {
            await ses.close()
        }
    }
    async insert(user_uuid, doc) {
        const ses = this.driver.session()
        doc.created = DateTime.fromStandardDate(new Date())
        doc.updated = DateTime.fromStandardDate(new Date())
        try {
            var q = `
            MATCH (p:Profile)-[:BELONGS_TO]->(u:User {uuid: $user_uuid})
            CREATE (v:Video $video_props)-[:CREATED_BY]->(p)
            `
            const result = await ses.run(
                q, { video_props: doc, user_uuid }
            )
            return process_query_array(result.records)
        } finally {
            await ses.close()
        }
    }
}
