import _ from 'lodash'
export function process_query_array(results: any) {
    return results.map(object => {
        let temp = object._fields[0].properties
        temp.id = object._fields[0].identity
        temp = _.omit(temp, 'password')
        return temp
    })
} 
export function process_query_single(record: any) {
    let temp = record._fields[0].properties
    temp.id = record._fields[0].identity
    temp = _.omit(temp, 'password')
    return temp
} 
export function process_query_single_password(record: any) {
    let temp = record._fields[0].properties
    temp.id = record._fields[0].identity
    return temp
} 