import { process_query_array} from './util'
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
import { v4 as uuidv4 } from 'uuid'
class query {
    label:String
    driver: any
    constructor(driver: any, label: String) {
        this.driver = driver
        this.label = label
    }

    async select() {
        const ses = this.driver.session()
        try {
            const result = await ses.run(
                `MATCH (objects:${this.label}) RETURN objects`
            )
            return process_query_array(result.records)
        } finally {
            await ses.close()
        }
    }
    async insert(doc) {
        const ses = this.driver.session()
        doc.uuid = uuidv4()
        doc.created = DateTime.fromStandardDate(new Date())
        doc.updated = DateTime.fromStandardDate(new Date())
        try {
            const result = await ses.run(
                `CREATE (n:${this.label} $props)`, {props: doc}
            )
            return process_query_array(result.records)
        } finally {
            await ses.close()
        }
    }
}

export default query