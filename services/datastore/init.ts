// This file hold the code to intialize the database
import neo4j from '../neo4j'
import log from '../logger'
import { CreateUser } from '../auth'
const session = neo4j.session()
const ns: String = 'Database'
const categories = [
    { name: 'Film & Animation' },
    { name: 'Entertainment' },
    { name: 'News & Politics' },
    { name: 'Howto & Style' },
    { name: 'Education' },
    { name: 'Science & Technology' },
    { name: 'Nonprofits & Activism' },
    { name: 'Religion' },
    { name: 'Food & Culinary' },
    { name: 'Books & Reading' },
    { name: 'Autos & Vehicles' },
    { name: 'Music' },
    { name: 'Pets & Animals' },
    { name: 'Sports' },
    { name: 'Hobby & Recreation' },
    { name: 'Travel & Events' },
    { name: 'Gaming' },
    { name: 'People & Blogs' },
    { name: 'Comedy' },
]
const constraints = [
    'CREATE CONSTRAINT unique_user_email IF NOT EXISTS ON(user:User) ASSERT user.email IS UNIQUE',
    'CREATE CONSTRAINT unique_user_phone IF NOT EXISTS ON(user:User) ASSERT user.phone IS UNIQUE',
    'CREATE CONSTRAINT unique_profile_username IF NOT EXISTS ON(p:Profile) ASSERT p.username IS UNIQUE',
    'CREATE CONSTRAINT unique_profile_uuid IF NOT EXISTS ON(p:Profile) ASSERT p.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_user_uuid IF NOT EXISTS ON(user:User) ASSERT user.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_video_uuid IF NOT EXISTS ON(video:Video) ASSERT video.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_category_name IF NOT EXISTS ON(category:Category) ASSERT category.name IS UNIQUE',
    'CREATE CONSTRAINT unique_hashtag_name IF NOT EXISTS ON(hashtag:Hashtag) ASSERT hashtag.name IS UNIQUE'
]
const category_query = `
UNWIND $categories AS category
MERGE (:Category {name: category.name})
`

async function createConstraints(){
    log.info(ns, 'Creating Database Constraints')
    for( const c of constraints){
        await session.run(c)
    }
}
async function createCategories(){
    log.info(ns, 'Creating Categories')
    await session.run(category_query, { categories })
}
export async function init (){
    await createConstraints()
    await createCategories()
    await CreateUser({email: 'admin', password: 'password'})
    log.info(ns, 'Database initialized')
}

