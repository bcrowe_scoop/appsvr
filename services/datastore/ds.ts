const { v4: uuidv4 } = require('uuid');
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
const _ = require('lodash');
interface RelationshipDefinition {
    /** Label of the node to create relationship to */
    label: String
    /** UUID of the node to create relationship to */
    uuid: String
    /** Boolean: True: created_node -> related_node, False: created_node <- related_node */
    outbound: Boolean
    /** Name of relationship. This is the name of the relationship */
    name: String

}
function map_records(records) {
    return records.map(r => {
        return r._fields[0]
    })
}

function map_record(records) {
    if (records.length > 0) {
        return records[0]._fields[0]

    }
    return {}
}

export class ds {
    driver: any;
    label: string;
    constructor(driver, label) {
        this.driver = driver
        this.label = label
    }
    async select() {
        const session = this.driver.session()
        const query = `
                MATCH (u:${this.label} )RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ], playlists:[ (u)--(p:TestPLaylist) | p {.*} ]}
            `
        try {
            var results = await session.run(query)
            return map_records(results.records)
        } finally {
            session.close()
        }
    }
    async selectOne(uuid: String) {
        const session = this.driver.session()
        const params = { uuid }
        const query = `
            MATCH (u:${this.label} {uuid: $uuid})RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ]}
            `
        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }
    }
    async insert(doc: any) {
        doc.uuid = uuidv4()
        var current_date = new Date()
        doc.created = DateTime.fromStandardDate(current_date)
        doc.updated = DateTime.fromStandardDate(current_date)
        const session = this.driver.session()
        const params = { doc }
        const query = `
                CREATE (u:${this.label} $doc) RETURN u{.*}
            `
        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }
    }
    async insertWithRelationship(doc: any, relationship: RelationshipDefinition) {
        // relationship reference
        // var relationship = {
        //     label: 'Label of the node to create relationship to',
        //     name: 'Name of relationship. This is the name of the relationship',
        //     uuid: 'uuid of the node to create relationship',
        //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
        // }
        doc.uuid = uuidv4()
        var current_date = new Date()
        doc.created = DateTime.fromStandardDate(current_date)
        doc.updated = DateTime.fromStandardDate(current_date)
        const session = this.driver.session()
        const params = { doc, uuid: relationship.uuid }
        var query = ''
        if (relationship.outbound) {
            query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${this.label} $doc)
                    CREATE (u)-[r:${relationship.name}]->(n)
                    RETURN u{.*}
                `
        } else {
            query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${this.label} $doc)
                    CREATE (u)<-[r:${relationship.name}]-(n)
                    RETURN u{.*}
                `
        }

        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }

    }
    async mergeWithRelationship(doc: any, relationship: RelationshipDefinition) {
        // relationship reference
        // var relationship = {
        //     label: 'Label of the node to create relationship to',
        //     name: 'Name of relationship. This is the name of the relationship',
        //     uuid: 'uuid of the node to create relationship',
        //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
        // }
        doc.uuid = uuidv4()
        var current_date = new Date()
        doc.created = DateTime.fromStandardDate(current_date)
        doc.updated = DateTime.fromStandardDate(current_date)
        const session = this.driver.session()
        const params = { doc, uuid: relationship.uuid }
        var query = ''
        if (relationship.outbound) {
            query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    MERGE (u:${this.label} $doc)
                    CREATE (u)-[r:${relationship.name}]->(n)
                    RETURN u{.*}
                `
        } else {
            query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    MERGE (u:${this.label} $doc)
                    CREATE (u)<-[r:${relationship.name}]-(n)
                    RETURN u{.*}
                `
        }

        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }

    }
    async customquery(query: string, params: any) {
        // relationship reference
        // var relationship = {
        //     label: 'Label of the node to create relationship to',
        //     name: 'Name of relationship. This is the name of the relationship',
        //     uuid: 'uuid of the node to create relationship',
        //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
        // }
        const session = this.driver.session()
        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }

    }
    async update(uuid: String, doc: any) {
        doc = _.omit(doc, 'uuid')
        var current_date = new Date()
        doc.updated = DateTime.fromStandardDate(current_date)
        const session = this.driver.session()
        const params = { doc, uuid }
        const query = `
                MATCH (p:${this.label} { uuid: $uuid })
                SET p += $doc
                RETURN p {.*}
            `
        try {
            var results = await session.run(query, params)
            return map_record(results.records)
        } finally {
            session.close()
        }
    }
    async delete(uuid: String) {
        const session = this.driver.session()
        const params = { uuid }
        const query = `
                MATCH (p:${this.label} { uuid: $uuid })
                DELETE p
            `
        try {
            var results = await session.run(query, params)
            return results
        } finally {
            session.close()
        }
    }
}
