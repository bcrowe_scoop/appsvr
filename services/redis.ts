import config from './config'
import redis from 'redis'
var redis_config = {
    host: config.redis.host,
    port: config.redis.port,
    retry_strategy: function(options) {
        if (options.error && options.error.code === "ECONNREFUSED") {
            // End reconnecting on a specific error and flush all commands with
            // a individual error
            return new Error("The server refused the connection");
        }
        if (options.total_retry_time > 1000 * 60 * 60) {
            // End reconnecting after a specific timeout and flush all commands
            // with a individual error
            return new Error("Retry time exhausted");
        }
        if (options.attempt > 10) {
            // End reconnecting with built in error
            return undefined;
        }
        // reconnect after
        return Math.min(options.attempt * 100, 3000);
    }
}

var redisClient = redis.createClient(redis_config)

module.exports = {
    redis_config,
    redisClient() {
        if (redisClient) {
            return redisClient
        }
        return redis.createClient(redis_config)
    }
}