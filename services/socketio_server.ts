import socket_io_server from 'socket.io'
import { createAdapter } from 'socket.io-redis'
import log from './logger'
import config from './config'
import jwt from 'jsonwebtoken'
const ns = 'SocketIO'
var io 
export default function (server){
    return new Promise((resolve, reject) => {
        if (!io) {
            io = new socket_io_server.Server(server, {
                cors: {
                    origin: 'http://localhost:8080',
                    methods: ["GET", "POST"],
                    allowedHeaders: ["token"],
                    credentials: true
                }
                
            })
            // SocketIO Authentication by JWT token.
            io.use((socket, next) => {
                try {
                    socket.decoded_token = jwt.verify(socket.handshake.headers.token, config.jwt_secret)
                } catch (err) {
                    console.error(`[SocketIO]: ${err.message}`)
                    return next(err)
                }
                return next()
            })

            // Attache Redis Adapter
            io.adapter(createAdapter(`redis://${config.redis.host}`))
            log.info('[Redis]',`[Redis] SocketIO connected to Redis`)

            // On Connection, subscribe to a room with user's uuid
            io.on('connection', socket => {
                log.info(ns, `New connection: email: ${socket.decoded_token.email}, uuid: ${socket.decoded_token.uuid}`)
                log.info(ns, `Email: ${socket.decoded_token.email} Joining room uuid: ${socket.decoded_token.uuid}`)
                socket.join(socket.decoded_token.uuid);
            })
            resolve(io)
        } else {
            resolve(io)
        }
    });
}