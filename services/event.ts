import event from 'events'
const emitter = new event.EventEmitter()
import { publish } from './rabbitmq'
import config from './config'

interface WorkerData {
    action: String;
    user_uuid: String;
    data?: any;
    timestamp: any
}
interface EventData {
    action: String;
    type: String;
    data?: any;
    timestamp: any
}

emitter.on('/video/uploaded', ({user_uuid, video}) => {

    // Create object for worker queue
    var worker_data: WorkerData = {
        action: 'process_video',
        user_uuid: user_uuid,
        data: video,
        timestamp: new Date()
        }
    publish('', config.rabbitmq.worker_queue, worker_data)
})

emitter.on('/video/deleted', ({user_uuid, video}) => {

    // Create object for worker queue
    var worker_data = {
            action: 'delete_video',
            user_uuid: user_uuid,
            data: video,
            timestamp: new Date()
        }

    console.log(`[AMQP] Sending job action: ${worker_data.action} for user_id: ${worker_data.user_uuid} on video: ${worker_data.data.uuid}`)
    publish('', config.rabbitmq.worker_queue, worker_data)
})
export default emitter