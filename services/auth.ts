import { UserTemplate, UserInterface } from '../models/user'
import {  ProfileTemplate, ProfileInterface } from '../models/profile'
import { uniqueNamesGenerator, adjectives, colors, animals, NumberDictionary, Config } from 'unique-names-generator'
import bcrypt from 'bcrypt'
import neo4j from './neo4j'
import log from './logger'
import { process_query_single, process_query_single_password } from './datastore/util'
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'

// Function to generate user
function GenerateProfileUsername(): String {
    const numberDictionary = NumberDictionary.generate({ min: 100, max: 999 });
    const customConfig: Config = {
        dictionaries: [adjectives, colors, animals, numberDictionary],
        separator: '_',
        length: 4,
    };
    const shortName: string = uniqueNamesGenerator(customConfig)
    return shortName
}
const numberDictionary = NumberDictionary.generate({ min: 100, max: 999 });
const customConfig: Config = {
    dictionaries: [adjectives, colors],
    separator: '-',
    length: 2,
};
const saltRounds = 10
interface Credentials {
    email: String;
    password: String
}
interface AuthenticationResults {
    user?: UserInterface,
    message?: string
    status: Boolean
}
interface DatabaseResults {
    data?: UserInterface,
    message?: string
    status: Boolean
}
export async function CreateUser(user: UserInterface): Promise<DatabaseResults> {
    var dbr = {
        status: true,
        message: '',
        data: null
    }
    var temp_user = Object.assign({}, UserTemplate)
    var temp_profile = Object.assign({}, ProfileTemplate)

    // Create User Object
    temp_user.email = user.email
    temp_user.password = bcrypt.hashSync(user.password, saltRounds)
    temp_user.uuid = uuidv4()
    let date = DateTime.fromStandardDate(new Date())
    temp_user.created = date
    temp_user.updated = date

    // Create Profile Object
    temp_profile.username = GenerateProfileUsername()
    temp_profile.uuid = uuidv4()
    temp_profile.created = date
    temp_profile.updated = date
    let session = neo4j.session()
    try {
        await session.run('CREATE (u:User $user_props)<-[:BELONGS_TO]-(p:Profile $profile_props)', { user_props: temp_user, profile_props: temp_profile})
        
    } catch (error) {
        log.error('Database', error.message)
        dbr.status = false
        dbr.message = error.message
    }
    finally {
        session.close()
    }
    return dbr
    
}

export async function Authenticate(credentials: Credentials): Promise<AuthenticationResults> {
    var returnData: AuthenticationResults = {status: false}
    let session = neo4j.session()
    let results = await session.run('MATCH ( u:User { email: $email } ) RETURN u', {email: credentials.email})
    if(results.records.length == 0){
        returnData.message = `Email ${credentials.email} not found`
        returnData.status = false

    }else {
        let user = process_query_single_password(results.records[0])
        if (bcrypt.compareSync(credentials.password, user.password)){
            returnData.message = `User ${credentials.email} successfully authenticated`
            returnData.status = true
            returnData.user = _.omit(user, 'password')
        } else {
            returnData.message = `Password is invalid`
            returnData.status = false
        }
    }
    return returnData
}

export async function ChangePassword(credentials: Credentials): Promise < DatabaseResults >{
    var dbr: DatabaseResults = {
        status: true,
        message:'',
        data: null
    }
    let session = neo4j.session()
    credentials.password = bcrypt.hashSync(credentials.password, saltRounds)
    let results = await session.run('MATCH ( u:User { email: $email } ) SET u.password = $password RETURN u', { email: credentials.email, password: credentials.password })
    if(results.records.length != 0){
        dbr.data = process_query_single(results.records[0])
        return dbr
    }else{
        return dbr
    }
}