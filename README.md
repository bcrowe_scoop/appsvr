## Scoop AppSRV02
-----------------
This repo holds the application server code for the scoop video sharing platform.

## Dependencies
* Typescript
* Express
* NEO4j
* GraphQL
* REDIS
* RABBITMQ

## Docker Commands
* Build
```
docker build -t bcrowe/scoop_server .
```

* Run
```
docker run -p 3000:3000 --env='DATABASE_HOST=172.16.1.155' --env='RABBITMQ_HOST=172.16.1.155' --env='REDIS_HOST=172.16.1.155' --env='MINIO_ENDPOINT=172.16.1.155' bcrowe/scoop_server
```

## Docker Environment Variables
The following varibles are available to configure the image
```
host: APP_HOST
port: APP_PORT
jwt_secret: APP_JWT_SECRET
api_auth: APP_API_AUTH
session_time: APP_SESSION_TIME
tmp_storage_directory: APP_TMP_STORAGE_DIRECTORY
media_service_provider:
  destination: MSP_DESTINATION
  image_base_path: MSP_IMAGE_BASE_PATH
  dash_base_path: MSP_DASH_BASE_PATH
  hls_base_path: MSP_HLS_BASE_PATH
minio:
  endPoint: MINIO_ENDPOINT
  port: MINIO_PORT
  useSSL: MINIO_USESSL
  accessKey: MINIO_ACCESSKEY
  secretKey: MINIO_SECRETKEY
  bucketName: MINIO_BUCKETNAME
  image_bucket: MINIO_IMAGE_BUCKET
redis:
  host: REDIS_HOST
  port: REDIS_PORT
rabbitmq:
  host: RABBITMQ_HOST
  port: RABBITMQ_PORT
  worker_queue: RABBITMQ_WORKER_QUEUE
database:
  host: DATABASE_HOST
  name: DATABASE_NAME
  port: DATABASE_PORT
  user: DATABASE_USER
  password: DATABASE_PASSWORD
```