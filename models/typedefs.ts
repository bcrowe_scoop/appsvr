export default `
    type Category {
        name: String!
        videos: [Video] @relation(name: "IN_CATEGORY", direction:IN)
    }
    type Hashtag {
        name: String!
    }
    type User {
        uuid: ID!
        email: String
        enabled: Boolean
        emailVerified: Boolean
        phoneVerified: Boolean
        role: String
        api_key: String
        stream_key: String
        profile: Profile @relation(name: "BELONGS_TO", direction:IN)
    }
    type VideoReaction @relation(name: "REACTED") {
        from: Profile
        to: Video
        status: String
    }
    type CommentReaction @relation(name: "REACTED") {
        from: Profile
        to: Comment
        status: String
    }
    type ReplyReaction @relation(name: "REACTED") {
        from: Profile
        to: Reply
        status: String
    }
    type Comment {
        uuid: ID!
        isYours: Boolean @cypher(statement: "MATCH (this)-[:CREATED_BY]->(p:Profile)-[:BELONGS_TO]->(u:User {uuid: $cypherParams.user_uuid}) WITH p, CASE WHEN p IS NULL THEN false ELSE true END as results RETURN results")
        content: String!
        like_status: String @cypher(statement:"MATCH (this)<-[likes:REACTED]-(:Profile)-[:BELONGS_TO]->(:User {uuid: $cypherParams.user_uuid})  RETURN likes.status")
        likes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'like'}]-(:Profile) RETURN COUNT(*)")
        dislikes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'dislike'}]-(:Profile) RETURN COUNT(*)")
        video: Video @relation(name: "BELONGS_TO", direction:OUT)
        profile:Profile @relation(name: "CREATED_BY", direction:OUT)
        replies: [Reply] @relation(name: "BELONGS_TO", direction:IN)
        created: DateTime
    }
    type Reply {
        uuid: ID!
        content: String!
        like_status: String @cypher(statement:"MATCH (this)<-[likes:REACTED]-(:Profile)-[:BELONGS_TO]->(:User {uuid: $cypherParams.user_uuid})  RETURN likes.status")
        likes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'like'}]-(:Profile) RETURN COUNT(*)")
        dislikes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'dislike'}]-(:Profile) RETURN COUNT(*)")
        isYours: Boolean @cypher(statement: "MATCH (this)-[:CREATED_BY]->(p:Profile)-[:BELONGS_TO]->(u:User {uuid: $cypherParams.user_uuid}) WITH p, CASE WHEN p IS NULL THEN false ELSE true END as results RETURN results")
        comment: Comment @relation(name: "BELONGS_TO", direction:OUT)
        profile:Profile @relation(name: "CREATED_BY", direction:OUT)
        created: DateTime
    }
    type Video {
        uuid: ID!
        title: String
        description: String
        allow_comments: Boolean
        allow_ratings: Boolean,
        like_status(profile_uuid: ID = ""): String @cypher(statement:"MATCH (this)<-[likes:REACTED]-(:Profile {uuid: $profile_uuid})  RETURN likes.status")
        created: DateTime
        updated: DateTime
        poster: String
        status: String
        thumbnails: [String]
        views: Int @cypher(statement: "MATCH (v:View)-[:BELONGS_TO]->(this) RETURN COUNT(v)")
        visibility: String
        profile: Profile @relation(name: "CREATED_BY", direction:OUT)
        hashtags: [Hashtag] @relation(name: "TAGGED_WITH", direction:OUT)
        category: Category @relation(name: "IN_CATEGORY", direction:OUT)
        likes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'like'}]-(:Profile) RETURN COUNT(*)")
        dislikes: Int @cypher(statement: "MATCH (this)<-[likes:REACTED {status:'dislike'}]-(:Profile) RETURN COUNT(*)")
        comments: [Comment] @relation(name: "BELONGS_TO", direction:IN)
    }
    type Profile {
        uuid: ID!
        username: String
        description: String
        avatar: String
        cover: String
        location: String
        feature_video: String
        display_feature_video: Boolean
        display_recent_videos: Boolean
        display_popular_videos: Boolean
        video_reactions: [VideoReaction]
        comment_reactions: [CommentReaction]
        reply_reactions: [ReplyReaction]
        history: [Watched]
        links: [String]
        created: String
        updated: String
        subscribed: Boolean @cypher(statement: "MATCH (this)<-[s:SUBSCRIBED_TO]-(:Profile)-[:BELONGS_TO]->(:User {uuid: $cypherParams.user_uuid }) WITH s, CASE WHEN s IS NULL THEN false ELSE true END as results RETURN results")
        videos: [Video] @relation(name: "CREATED_BY", direction:IN)
        subscribers: [Profile] @relation(name: "SUBSCRIBED_TO", direction:IN)
        subscriptions: [Profile] @relation(name: "SUBSCRIBED_TO", direction:OUT)
    }
    type Watched @relation(name: "WATCHED"){
        from: Profile
        to: Video
        when: DateTime!
    }
    type Query {
        UserProfile:Profile @cypher(statement: """ 
            MATCH (:User {uuid: $cypherParams.user_uuid})--(p:Profile) RETURN p
        """)
    }
    type Mutation {
        AddHashtagToVideo(video_uuid: ID!, hashtag_name: String!):Hashtag
        SetCategoryToVideo(video_uuid: ID!, category_name: String!):Video
        ReactToVideo(video_uuid: ID!, profile_uuid: ID!, like_status: String!):Video
        ReactToComment(comment_uuid: ID!, profile_uuid: ID!, like_status: String!):Comment
        CommentOnVideo(video_uuid: ID!, profile_uuid: ID!, content: String!):Video
        ReplyToComment(comment_uuid: ID!, profile_uuid: ID!, content: String!):Comment
        DeleteCommentFromVideo(video_uuid:ID!, comment_uuid: ID!):Comment
        DeleteReplyFromComment(reply_uuid:ID!, comment_uuid: ID!):Reply
        WatchVideo(profile_uuid: String!, video_uuid: String!): Video
    }
`