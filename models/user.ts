import { gql } from 'apollo-server-express'
export interface UserInterface {
    email: String;
    uuid?: String;
    password: String;
    enabled?: Boolean;
    emailVerified?: Boolean;
    phoneVerified?: Boolean;
    phone?: String;
    role?: String;
    api_key?: String;
    stream_key?: String;
    created?: any;
    updated?: any;
}

export var UserTemplate: UserInterface = {
    email:'',
    password:'',
    enabled: true,
    emailVerified: false,
    phoneVerified: false,
    role: '',
    api_key: '',
    stream_key: ''
}
export const typeDef = gql`
    type User {
        email: String
        enabled: Boolean
        emailVerified: Boolean
        phoneVerified: Boolean
        role: String
        api_key: String
        stream_key: String
        profile: Profile
    }
`