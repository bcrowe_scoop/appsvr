import { gql } from 'apollo-server-express'
export interface ProfileInterface {
    uuid?: String;
    username: String;
    description?: String;
    avatar?: String;
    cover?: String;
    feature_video?: String;
    links?: [String?]
    created?: any;
    updated?: any;
}

export var ProfileTemplate: ProfileInterface = {
    uuid: '',
    username: '',
    description: '',
    avatar: '',
    cover: '',
    feature_video: '',
    links: [],
    created: '',
    updated: '',
}

export const typeDef = gql`
    type Profile {
        uuid: ID!
        username: String
        description: String
        avatar: String
        cover: String
        feature_video: String
        links: [String]
        created: String
        updated: String
    }
`