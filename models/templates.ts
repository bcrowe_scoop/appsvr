export var user = {
    enabled: true,
    emailVerified: false,
    phoneVerified: false,
    phone: '',
    role: 'user',
    api_key: '',
    stream_key: ''
}

export var profile = {
    username: '',
    description: '',
    location: '',
    avatar: '',
    links: [],
    profiles: [],
}