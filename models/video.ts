import { gql } from 'apollo-server-express'

export var VideoTemplate = {
    uuid: '',
    title: '',
    description: '',
    hashtags: [],
    visibility: 'draft',
    status: 'processing',
    poster: '',
    allow_comments: true,
    allow_ratings: true,
    category_id: '',
    thumbnails: []
}

export const typeDef = gql`
    type Video {
        uuid: ID
        title: String
        description: String
        allow_comments: Boolean
        allow_ratings: Boolean
        created: String
        updated: String
        poster: String
        status: String
        thumbnails: [String]
        views: Int
        visibility: String
        profile: Profile
    }

    type Query {
        Videos(uuid: String): [Video]
    }
`