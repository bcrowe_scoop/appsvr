import express from 'express'
const router = express.Router()
import users from './users'
import videos from './videos'
import profile from './profile'
router.use('/users', users)
router.use('/videos', videos)
router.use('/profile', profile)



export default router