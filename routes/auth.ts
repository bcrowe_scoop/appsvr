import express from 'express'
const router = express.Router()
import passport from 'passport'
import jwt from 'jsonwebtoken'
import config from '../services/config'
import { Authenticate, ChangePassword, CreateUser } from '../services/auth'
import { validate, ValidationError, Joi } from 'express-validation'
// Login
const loginValidation = {
    body: Joi.object({
        email: Joi.string().trim(),
        password: Joi.string()
    })
}
router.post('/login', validate(loginValidation, {}, {}), passport.authenticate('local', { session: false }), (req, res) => {
    // generate a signed son web token with the contents of user object and return it in the response
    const token = jwt.sign(req.user, config.jwt_secret, { expiresIn: config.session_time });
    return res.json({ user:req.user, token });
})

router.post('/logout', async (req, res) => {
    req.logout()
    res.json({status: true, message: 'User logged out'})
})

// Signup User validation code
const signupValidation = {
    body: Joi.object({
        email: Joi.string().trim().email(),
        password: Joi.string().trim()
    })
}

// Signup User Endpoint               
router.post('/signup', validate(signupValidation, {}, {}), async (req, res) => {
    try {
        let results = await CreateUser({email: req.body.email, password: req.body.password})
        res.json(results)
    } catch (error) {
        res.status(500).json({error: error.toString()})
    }
})

// Signup User validation code
const changepasswordValidation = {
    body: Joi.object({
        email: Joi.string().trim().email(),
        password: Joi.string().trim()
    })
}
router.post('/changepassword', validate(signupValidation, {}, {}), async (req, res) => {
    try {
        let results = await ChangePassword({email: req.body.email, password: req.body.password})
        res.json(results)
    } catch (error) {
        res.status(500).json({error: error.toString()})
    }
})

router.post('/user', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json(req.user)
})

export default router