import express from 'express'
const router = express.Router();
import { v4 as uuidv4 } from 'uuid'
import { DateTime } from 'neo4j-driver/lib/temporal-types.js'
import { ds } from '../services/datastore/ds'

router.post('/:video_uuid', async (req, res) => {
    var view = new ds(req.app.locals.neo4j, 'Views')
    var query = `
    MATCH ( video:Video {uuid: $video_uuid} )
    CREATE ( view:View $view )-[:BELONGS_TO]->( video )
    `
    const params = {
        view:{
            uuid:uuidv4(),
            created: DateTime.fromStandardDate(new Date()),
            host: req.headers.host,
            x_forwarded_for: req.headers['x-forwarded-for'],
            x_real_ip: req.headers['x-real-ip'] || req.connection.remoteAddress
        },
        video_uuid: req.params.video_uuid
    }
    
    const results = await view.customquery(query, params)
    res.json(results)
})

export default router

