import express from 'express'
const router = express.Router()
import ds from '../services/datastore'

// Route Endpoints
router.get('/', async (req, res) => {
    const users = await ds.user.select()
    console.log(users)
    res.json(users)
})

router.post('/', async (req, res) => {
    const newuser = await ds.user.insert(req.body)
    res.json(newuser)

})

export default router