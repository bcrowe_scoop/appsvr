import express from 'express'
const router = express.Router();
import config from '../services/config'

// This endpoint provides the urls info to fetch images and video
router.get('/', (req, res) => {
    res.json(config.media_service_provider)
})

export default router