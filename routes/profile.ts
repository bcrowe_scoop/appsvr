import express from 'express'
const router = express.Router();
import config from '../services/config'
import event_bus from'../services/event'
import multer from 'multer';
import minio from '../services/minio_actions'
import path from 'path'
import { v4 as uuidv4 } from 'uuid'
import { VideoTemplate } from '../models/video'
import {ds} from '../services/datastore/ds'
import Jimp from 'jimp/es'
import { unlinkSync } from 'fs';
// Insert new video to the user account

var disk_storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config.tmp_storage_directory)
    },
    filename: function (req, file, cb) {
        const new_filename = uuidv4()
        // @ts-ignore
        file.uuid = new_filename
        cb(null, `${new_filename}-original${path.extname(file.originalname)}`)
    }
})
var upload = multer({ storage: disk_storage }).single('avatar')

router.post('/avatar', upload, async (req, res) => {
    // @ts-ignore
    const new_file_name = `${req.file.uuid}${path.extname(req.file.originalname)}`
    const new_file_path = path.join(config.tmp_storage_directory, new_file_name)
    // JIMP Read Image
    var image = await Jimp.read(req.file.path)

    // Jimp Process and write imate
    // @ts-ignore
    await image.resize(250, 250).writeAsync(new_file_path)
    
    // Write to minio
    // @ts-ignore
    await minio.putObject(new_file_name, config.minio.image_bucket, new_file_path, {mimetype: req.file.mimetype})

    // Update Profile in Neo4J
    const Profile = new ds(req.app.locals.neo4j, 'Profile')
    const query = `
        MATCH (p:Profile)--(:User {uuid: $user_uuid})
        SET p.avatar = $avatar
        RETURN p
    `
    // @ts-ignore
    var results = await Profile.customquery(query, { user_uuid: req.user.uuid, avatar: new_file_name})

    // Remove original file and processed file
    unlinkSync(req.file.path)
    unlinkSync(new_file_path)
    // Return resutls
    res.json(results)
})
router.post('/cover', upload, async (req, res) => {

    var image = await Jimp.read(req.file.path)
    await image.scaleToFit(50, 50).writeAsync(req.file.path)
    await minio.putObject(req.file.filename, config.minio.image_bucket, req.file.path, {mimetype: req.file.mimetype})
    const Profile = new ds(req.app.locals.neo4j, 'Profile')
    const query = `
        MATCH (p:Profile)--(:User {uuid: $user_uuid})
        SET p.cover = $cover
        RETURN p
    `
    // @ts-ignore
    var results = await Profile.customquery(query, { user_uuid: req.user.uuid, cover: req.file.filename})
    res.json(results)
})


export default router