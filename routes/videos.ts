import express from 'express'
const router = express.Router();
import config from '../services/config'
import event_bus from'../services/event'
import { minioClient } from '../services/minio'
import multer from 'multer';
import multer_minio from 'multer-minio-storage-engine';
import path from 'path'
import { v4 as uuidv4 } from 'uuid'
import { VideoTemplate } from '../models/video'
import { Video } from '../services/datastore/video'
// Insert new video to the user account

var minio_storage = multer_minio({
    minio: minioClient,
    bucketName: config.minio.bucketName,
    metaData: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname })
    },
    objectName: function (req, file, cb) {
        file.uuid = uuidv4()
        cb(null, `${file.uuid}-original${path.extname(file.originalname)}`)

    }
})
var upload = multer({ storage: minio_storage }).single('video')

router.post('/', upload, async (req, res) => {
    let v = new Video(req.app.locals.neo4j)
    var temp_video = Object.assign({}, VideoTemplate)
    // @ts-ignore
    temp_video.uuid = req.file.uuid 
    // @ts-ignore
    await v.insert(req.user.uuid, temp_video)
    // @ts-ignore
    event_bus.emit('/video/uploaded', { user_uuid: req.user.uuid, video: req.file})
    res.json(req.file)
})


export default router