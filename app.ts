import express from 'express'
import routes from './routes'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import './services/passport'
import passport from 'passport'
import auth from './routes/auth'
import video_analytics from './routes/video_analytics'
import media_service_provider from './routes/media_service_provider'
import { apolloserver } from './services/graphql/server'
const app = express();
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

apolloserver.applyMiddleware({app})

app.use('/api', passport.authenticate('jwt', { session: false }), routes)
app.use('/auth', auth)
app.use('/video_analytics', video_analytics)
app.use('/media_service_provider', media_service_provider)
app.get('/', (req, res) => {
    res.json({ message: 'This is typescript' })
})


export default app