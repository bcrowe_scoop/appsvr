import { createServer } from 'http' 
import config from './services/config'
import app from './app'
import { init } from './services/datastore/init'
import log from './services/logger'
import driver from './services/neo4j'
import socketio_server from './services/socketio_server'
import { start as StartRabbitMQ } from './services/rabbitmq'
const ns: String = 'Server'
async function start(){
    await init()
    var server = createServer(app)
    await StartRabbitMQ()
    app.locals.neo4j = driver
    server.listen(config.port, () => {
        log.info(ns, `Listening on port: ${config.port}`)
    })
    await socketio_server(server)
}

// Start the server
start()