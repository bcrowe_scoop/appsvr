"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const config_1 = __importDefault(require("./services/config"));
const app_1 = __importDefault(require("./app"));
const init_1 = require("./services/datastore/init");
const logger_1 = __importDefault(require("./services/logger"));
const neo4j_1 = __importDefault(require("./services/neo4j"));
const socketio_server_1 = __importDefault(require("./services/socketio_server"));
const rabbitmq_1 = require("./services/rabbitmq");
const ns = 'Server';
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        yield init_1.init();
        var server = http_1.createServer(app_1.default);
        yield rabbitmq_1.start();
        app_1.default.locals.neo4j = neo4j_1.default;
        server.listen(config_1.default.port, () => {
            logger_1.default.info(ns, `Listening on port: ${config_1.default.port}`);
        });
        yield socketio_server_1.default(server);
    });
}
// Start the server
start();
