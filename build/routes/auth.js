"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const passport_1 = __importDefault(require("passport"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../services/config"));
const auth_1 = require("../services/auth");
const express_validation_1 = require("express-validation");
// Login
const loginValidation = {
    body: express_validation_1.Joi.object({
        email: express_validation_1.Joi.string().trim(),
        password: express_validation_1.Joi.string()
    })
};
router.post('/login', express_validation_1.validate(loginValidation, {}, {}), passport_1.default.authenticate('local', { session: false }), (req, res) => {
    // generate a signed son web token with the contents of user object and return it in the response
    const token = jsonwebtoken_1.default.sign(req.user, config_1.default.jwt_secret, { expiresIn: config_1.default.session_time });
    return res.json({ user: req.user, token });
});
router.post('/logout', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    req.logout();
    res.json({ status: true, message: 'User logged out' });
}));
// Signup User validation code
const signupValidation = {
    body: express_validation_1.Joi.object({
        email: express_validation_1.Joi.string().trim().email(),
        password: express_validation_1.Joi.string().trim()
    })
};
// Signup User Endpoint               
router.post('/signup', express_validation_1.validate(signupValidation, {}, {}), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let results = yield auth_1.CreateUser({ email: req.body.email, password: req.body.password });
        res.json(results);
    }
    catch (error) {
        res.status(500).json({ error: error.toString() });
    }
}));
// Signup User validation code
const changepasswordValidation = {
    body: express_validation_1.Joi.object({
        email: express_validation_1.Joi.string().trim().email(),
        password: express_validation_1.Joi.string().trim()
    })
};
router.post('/changepassword', express_validation_1.validate(signupValidation, {}, {}), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let results = yield auth_1.ChangePassword({ email: req.body.email, password: req.body.password });
        res.json(results);
    }
    catch (error) {
        res.status(500).json({ error: error.toString() });
    }
}));
router.post('/user', passport_1.default.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json(req.user);
});
exports.default = router;
