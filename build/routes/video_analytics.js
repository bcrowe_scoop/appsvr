"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const uuid_1 = require("uuid");
const temporal_types_js_1 = require("neo4j-driver/lib/temporal-types.js");
const ds_1 = require("../services/datastore/ds");
router.post('/:video_uuid', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var view = new ds_1.ds(req.app.locals.neo4j, 'Views');
    var query = `
    MATCH ( video:Video {uuid: $video_uuid} )
    CREATE ( view:View $view )-[:BELONGS_TO]->( video )
    `;
    const params = {
        view: {
            uuid: uuid_1.v4(),
            created: temporal_types_js_1.DateTime.fromStandardDate(new Date()),
            host: req.headers.host,
            x_forwarded_for: req.headers['x-forwarded-for'],
            x_real_ip: req.headers['x-real-ip'] || req.connection.remoteAddress
        },
        video_uuid: req.params.video_uuid
    };
    const results = yield view.customquery(query, params);
    res.json(results);
}));
exports.default = router;
