"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const config_1 = __importDefault(require("../services/config"));
// This endpoint provides the urls info to fetch images and video
router.get('/', (req, res) => {
    res.json(config_1.default.media_service_provider);
});
exports.default = router;
