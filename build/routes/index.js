"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const users_1 = __importDefault(require("./users"));
const videos_1 = __importDefault(require("./videos"));
const profile_1 = __importDefault(require("./profile"));
router.use('/users', users_1.default);
router.use('/videos', videos_1.default);
router.use('/profile', profile_1.default);
exports.default = router;
