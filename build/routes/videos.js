"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const config_1 = __importDefault(require("../services/config"));
const event_1 = __importDefault(require("../services/event"));
const minio_1 = require("../services/minio");
const multer_1 = __importDefault(require("multer"));
const multer_minio_storage_engine_1 = __importDefault(require("multer-minio-storage-engine"));
const path_1 = __importDefault(require("path"));
const uuid_1 = require("uuid");
const video_1 = require("../models/video");
const video_2 = require("../services/datastore/video");
// Insert new video to the user account
var minio_storage = multer_minio_storage_engine_1.default({
    minio: minio_1.minioClient,
    bucketName: config_1.default.minio.bucketName,
    metaData: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname });
    },
    objectName: function (req, file, cb) {
        file.uuid = uuid_1.v4();
        cb(null, `${file.uuid}-original${path_1.default.extname(file.originalname)}`);
    }
});
var upload = multer_1.default({ storage: minio_storage }).single('video');
router.post('/', upload, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let v = new video_2.Video(req.app.locals.neo4j);
    var temp_video = Object.assign({}, video_1.VideoTemplate);
    // @ts-ignore
    temp_video.uuid = req.file.uuid;
    // @ts-ignore
    yield v.insert(req.user.uuid, temp_video);
    // @ts-ignore
    event_1.default.emit('/video/uploaded', { user_uuid: req.user.uuid, video: req.file });
    res.json(req.file);
}));
exports.default = router;
