"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const config_1 = __importDefault(require("../services/config"));
const multer_1 = __importDefault(require("multer"));
const minio_actions_1 = __importDefault(require("../services/minio_actions"));
const path_1 = __importDefault(require("path"));
const uuid_1 = require("uuid");
const ds_1 = require("../services/datastore/ds");
const es_1 = __importDefault(require("jimp/es"));
const fs_1 = require("fs");
// Insert new video to the user account
var disk_storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.default.tmp_storage_directory);
    },
    filename: function (req, file, cb) {
        const new_filename = uuid_1.v4();
        // @ts-ignore
        file.uuid = new_filename;
        cb(null, `${new_filename}-original${path_1.default.extname(file.originalname)}`);
    }
});
var upload = multer_1.default({ storage: disk_storage }).single('avatar');
router.post('/avatar', upload, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // @ts-ignore
    const new_file_name = `${req.file.uuid}${path_1.default.extname(req.file.originalname)}`;
    const new_file_path = path_1.default.join(config_1.default.tmp_storage_directory, new_file_name);
    // JIMP Read Image
    var image = yield es_1.default.read(req.file.path);
    // Jimp Process and write imate
    // @ts-ignore
    yield image.resize(250, 250).writeAsync(new_file_path);
    // Write to minio
    // @ts-ignore
    yield minio_actions_1.default.putObject(new_file_name, config_1.default.minio.image_bucket, new_file_path, { mimetype: req.file.mimetype });
    // Update Profile in Neo4J
    const Profile = new ds_1.ds(req.app.locals.neo4j, 'Profile');
    const query = `
        MATCH (p:Profile)--(:User {uuid: $user_uuid})
        SET p.avatar = $avatar
        RETURN p
    `;
    // @ts-ignore
    var results = yield Profile.customquery(query, { user_uuid: req.user.uuid, avatar: new_file_name });
    // Remove original file and processed file
    fs_1.unlinkSync(req.file.path);
    fs_1.unlinkSync(new_file_path);
    // Return resutls
    res.json(results);
}));
router.post('/cover', upload, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var image = yield es_1.default.read(req.file.path);
    yield image.scaleToFit(50, 50).writeAsync(req.file.path);
    yield minio_actions_1.default.putObject(req.file.filename, config_1.default.minio.image_bucket, req.file.path, { mimetype: req.file.mimetype });
    const Profile = new ds_1.ds(req.app.locals.neo4j, 'Profile');
    const query = `
        MATCH (p:Profile)--(:User {uuid: $user_uuid})
        SET p.cover = $cover
        RETURN p
    `;
    // @ts-ignore
    var results = yield Profile.customquery(query, { user_uuid: req.user.uuid, cover: req.file.filename });
    res.json(results);
}));
exports.default = router;
