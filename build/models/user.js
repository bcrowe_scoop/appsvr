"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeDef = exports.UserTemplate = void 0;
const apollo_server_express_1 = require("apollo-server-express");
exports.UserTemplate = {
    email: '',
    password: '',
    enabled: true,
    emailVerified: false,
    phoneVerified: false,
    role: '',
    api_key: '',
    stream_key: ''
};
exports.typeDef = apollo_server_express_1.gql `
    type User {
        email: String
        enabled: Boolean
        emailVerified: Boolean
        phoneVerified: Boolean
        role: String
        api_key: String
        stream_key: String
        profile: Profile
    }
`;
