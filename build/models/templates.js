"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.profile = exports.user = void 0;
exports.user = {
    enabled: true,
    emailVerified: false,
    phoneVerified: false,
    phone: '',
    role: 'user',
    api_key: '',
    stream_key: ''
};
exports.profile = {
    username: '',
    description: '',
    location: '',
    avatar: '',
    links: [],
    profiles: [],
};
