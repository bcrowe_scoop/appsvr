"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeDef = exports.VideoTemplate = void 0;
const apollo_server_express_1 = require("apollo-server-express");
exports.VideoTemplate = {
    uuid: '',
    title: '',
    description: '',
    hashtags: [],
    visibility: 'draft',
    status: 'processing',
    poster: '',
    allow_comments: true,
    allow_ratings: true,
    category_id: '',
    thumbnails: []
};
exports.typeDef = apollo_server_express_1.gql `
    type Video {
        uuid: ID
        title: String
        description: String
        allow_comments: Boolean
        allow_ratings: Boolean
        created: String
        updated: String
        poster: String
        status: String
        thumbnails: [String]
        views: Int
        visibility: String
        profile: Profile
    }

    type Query {
        Videos(uuid: String): [Video]
    }
`;
