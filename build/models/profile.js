"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeDef = exports.ProfileTemplate = void 0;
const apollo_server_express_1 = require("apollo-server-express");
exports.ProfileTemplate = {
    uuid: '',
    username: '',
    description: '',
    avatar: '',
    cover: '',
    feature_video: '',
    links: [],
    created: '',
    updated: '',
};
exports.typeDef = apollo_server_express_1.gql `
    type Profile {
        uuid: ID!
        username: String
        description: String
        avatar: String
        cover: String
        feature_video: String
        links: [String]
        created: String
        updated: String
    }
`;
