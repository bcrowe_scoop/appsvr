"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.apolloserver = void 0;
// Graphql
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config"));
const neo4j_graphql_js_1 = require("neo4j-graphql-js");
const apollo_server_express_1 = require("apollo-server-express");
const typedefs_1 = __importDefault(require("../../models/typedefs"));
const neo4j_1 = __importDefault(require("../neo4j"));
const resolvers_1 = require("./resolvers");
function verifyToken(auth_header) {
    if (auth_header && typeof auth_header != 'undefined') {
        var parts = auth_header.split(' ');
        if (parts.length === 2) {
            if (parts[0] == 'Bearer') {
                try {
                    var user = jsonwebtoken_1.default.verify(parts[1], config_1.default.jwt_secret);
                    return user;
                }
                catch (error) {
                    console.log(error);
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }
    else {
        return false;
    }
}
const schema = neo4j_graphql_js_1.makeAugmentedSchema({
    typeDefs: typedefs_1.default,
    resolvers: resolvers_1.resolvers
});
exports.apolloserver = new apollo_server_express_1.ApolloServer({
    schema,
    context({ req }) {
        var user = verifyToken(req.headers.authorization);
        return {
            req,
            driver: neo4j_1.default,
            user,
            cypherParams: {
                user_uuid: user.uuid
            }
        };
    }
});
