"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = void 0;
const neo4j_graphql_js_1 = require("neo4j-graphql-js");
const temporal_types_js_1 = require("neo4j-driver/lib/temporal-types.js");
const logger_1 = __importDefault(require("../logger"));
const event_1 = __importDefault(require("../event"));
const uuid_1 = require("uuid");
const ds_1 = require("../datastore/ds");
exports.resolvers = {
    Mutation: {
        SetCategoryToVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query1 = `
                MATCH (v:Video { uuid: $video_uuid} )-[r:IN_CATEGORY]->(:Category) DELETE r
            `;
                const query2 = `
                MATCH (v2:Video {uuid: $video_uuid}), (c:Category {name: $category_name})
                MERGE (v2)-[r2:IN_CATEGORY]->(c) RETURN v2{.*}
            `;
                const data = {
                    video_uuid: params.video_uuid,
                    category_name: params.category_name
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    yield d.customquery(query1, data);
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        WatchVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query1 = `
                MATCH (:Profile { uuid: $profile_uuid } )-[watched:WATCHED]->(:Video { uuid: $video_uuid } ) DELETE watched
            `;
                const query2 = `
                MATCH (profile:Profile { uuid: $profile_uuid } ), (video:Video { uuid: $video_uuid } )
                MERGE (profile)-[:WATCHED { when: $when}]->(video) RETURN video{.*}
            `;
                const data = {
                    video_uuid: params.video_uuid,
                    profile_uuid: params.profile_uuid,
                    when: temporal_types_js_1.DateTime.fromStandardDate(new Date())
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    yield d.customquery(query1, data);
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        ReactToVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query1 = `
                MATCH ( video:Video { uuid: $video_uuid} )<-[likes:REACTED]-(:Profile {uuid: $profile_uuid}) DELETE likes
            `;
                const query2 = `
                MATCH ( video:Video { uuid: $video_uuid} ), ( profile:Profile { uuid: $profile_uuid } )
                MERGE (video)<-[:REACTED {status: $like_status}]-(profile) RETURN video{.*}
            `;
                const data = {
                    video_uuid: params.video_uuid,
                    profile_uuid: params.profile_uuid,
                    like_status: params.like_status
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    yield d.customquery(query1, data);
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        ReactToComment(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query1 = `
                MATCH ( comment:Comment { uuid: $comment_uuid} )<-[likes:REACTED]-(:Profile {uuid: $profile_uuid}) DELETE likes
            `;
                const query2 = `
                MATCH ( comment:Comment { uuid: $comment_uuid} ), ( profile:Profile { uuid: $profile_uuid } )
                MERGE (comment)<-[:REACTED {status: $like_status}]-(profile) RETURN comment{.*}
            `;
                const data = {
                    comment_uuid: params.comment_uuid,
                    profile_uuid: params.profile_uuid,
                    like_status: params.like_status
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    yield d.customquery(query1, data);
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        CommentOnVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query2 = `
                MATCH (p:Profile {uuid: $profile_uuid }), (v:Video { uuid: $video_uuid })
                CREATE(c:Comment {uuid: $new_uuid, content: $content, created: $created})
                CREATE (p)<-[:CREATED_BY]-(c)-[:BELONGS_TO]->(v)
                RETURN v{.*}
            `;
                const data = {
                    video_uuid: params.video_uuid,
                    profile_uuid: params.profile_uuid,
                    content: params.content,
                    new_uuid: uuid_1.v4(),
                    created: temporal_types_js_1.DateTime.fromStandardDate(new Date())
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        ReplyToComment(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query2 = `
                MATCH (p:Profile {uuid: $profile_uuid }), (c:Comment { uuid: $comment_uuid })
                CREATE(r:Reply {uuid: $new_uuid, content: $content})
                CREATE (p)<-[:CREATED_BY]-(r)-[:BELONGS_TO]->(c)
                RETURN c{.*}
            `;
                const data = {
                    comment_uuid: params.comment_uuid,
                    profile_uuid: params.profile_uuid,
                    content: params.content,
                    new_uuid: uuid_1.v4(),
                    created: temporal_types_js_1.DateTime.fromStandardDate(new Date())
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    var results2 = yield d.customquery(query2, data);
                }
                finally {
                }
                return results2;
            });
        },
        DeleteCommentFromVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query2 = `
                MATCH (v:Video {uuid: $video_uuid}), (c:Comment {uuid: $comment_uuid})
                WITH c, c.uuid as uuid
                DETACH DELETE c
                RETURN {uuid: uuid}
            `;
                const data = {
                    comment_uuid: params.comment_uuid,
                    video_uuid: params.video_uuid,
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    var results2 = yield d.customquery(query2, data);
                    console.log(results2);
                }
                finally {
                }
                return results2;
            });
        },
        DeleteReplyFromComment(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const query2 = `
                MATCH (r:Reply {uuid: $reply_uuid})
                WITH r, r.uuid as uuid
                DETACH DELETE r
                RETURN {uuid: uuid}
            `;
                const data = {
                    comment_uuid: params.comment_uuid,
                    reply_uuid: params.reply_uuid,
                };
                try {
                    const d = new ds_1.ds(ctx.driver, 'Video');
                    var results2 = yield d.customquery(query2, data);
                    console.log(results2);
                }
                finally {
                }
                return results2;
            });
        },
        DeleteVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const VideoStore = new ds_1.ds(ctx.driver, 'Video');
                    var video = yield VideoStore.selectOne(params.uuid);
                    var results = yield neo4j_graphql_js_1.neo4jgraphql(object, params, ctx, resolveInfo);
                }
                finally {
                    logger_1.default.info('Event', `Emitting event: '/video/deleted' for video: ${params.uuid} `);
                    event_1.default.emit('/video/deleted', { user_uuid: ctx.user.uuid, video });
                    console.log(ctx);
                }
                return results;
            });
        },
        AddHashtagToVideo(object, params, ctx, resolveInfo) {
            return __awaiter(this, void 0, void 0, function* () {
                const HashtagStore = new ds_1.ds(ctx.driver, 'Hashtag');
                const hashtag_name = String(params.hashtag_name).toLowerCase();
                const query = `
                MATCH (v:Video {uuid: $uuid})
                MERGE (h:Hashtag {name: $name})
                MERGE (h)<-[r:TAGGED_WITH]-(v)
                RETURN h{.*}
            `;
                var Hashtag = yield HashtagStore.customquery(query, { name: hashtag_name, uuid: params.video_uuid });
                return Hashtag;
            });
        },
    }
};
