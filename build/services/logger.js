"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    info(namespace, message) {
        let d = new Date().toISOString();
        console.info(`[${d}] [${namespace}]: ${message}`);
    },
    error(namespace, message) {
        let d = new Date().toISOString();
        console.error(`[${d}] [${namespace}]: ${message}`);
    },
    warning(namespace, message) {
        let d = new Date().toISOString();
        console.warn(`[${d}] [${namespace}]: ${message}`);
    },
    debug(namespace, message) {
        let d = new Date().toISOString();
        console.debug(`[${d}] [${namespace}]: ${message}`);
    },
};
