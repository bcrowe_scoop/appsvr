"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const config_1 = __importDefault(require("./config"));
const passport_local_1 = require("passport-local");
const passport_jwt_1 = __importDefault(require("passport-jwt"));
const auth_1 = require("./auth");
const logger_1 = __importDefault(require("./logger"));
logger_1.default.info('Server', 'Setting up passport for authentication.');
const JWTStrategy = passport_jwt_1.default.Strategy;
const ExtractJWT = passport_jwt_1.default.ExtractJwt;
passport_1.default.use(new passport_local_1.Strategy({
    usernameField: 'email',
    passwordField: 'password'
}, function (email, password, cb) {
    // Custom Authentication Promise using auth service
    auth_1.Authenticate({ email, password }).then(auth_results => {
        if (!auth_results.status) {
            return cb(null, auth_results.status, { message: auth_results.message });
        }
        return cb(null, auth_results.user, { message: auth_results.message });
    }).catch(err => cb(err));
}));
passport_1.default.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: config_1.default.jwt_secret
}, function (jwtPayload, cb) {
    //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
    return cb(null, jwtPayload);
}));
