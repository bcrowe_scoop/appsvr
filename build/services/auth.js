"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangePassword = exports.Authenticate = exports.CreateUser = void 0;
const user_1 = require("../models/user");
const profile_1 = require("../models/profile");
const unique_names_generator_1 = require("unique-names-generator");
const bcrypt_1 = __importDefault(require("bcrypt"));
const neo4j_1 = __importDefault(require("./neo4j"));
const logger_1 = __importDefault(require("./logger"));
const util_1 = require("./datastore/util");
const temporal_types_js_1 = require("neo4j-driver/lib/temporal-types.js");
const uuid_1 = require("uuid");
const lodash_1 = __importDefault(require("lodash"));
// Function to generate user
function GenerateProfileUsername() {
    const numberDictionary = unique_names_generator_1.NumberDictionary.generate({ min: 100, max: 999 });
    const customConfig = {
        dictionaries: [unique_names_generator_1.adjectives, unique_names_generator_1.colors, unique_names_generator_1.animals, numberDictionary],
        separator: '_',
        length: 4,
    };
    const shortName = unique_names_generator_1.uniqueNamesGenerator(customConfig);
    return shortName;
}
const numberDictionary = unique_names_generator_1.NumberDictionary.generate({ min: 100, max: 999 });
const customConfig = {
    dictionaries: [unique_names_generator_1.adjectives, unique_names_generator_1.colors],
    separator: '-',
    length: 2,
};
const saltRounds = 10;
function CreateUser(user) {
    return __awaiter(this, void 0, void 0, function* () {
        var dbr = {
            status: true,
            message: '',
            data: null
        };
        var temp_user = Object.assign({}, user_1.UserTemplate);
        var temp_profile = Object.assign({}, profile_1.ProfileTemplate);
        // Create User Object
        temp_user.email = user.email;
        temp_user.password = bcrypt_1.default.hashSync(user.password, saltRounds);
        temp_user.uuid = uuid_1.v4();
        let date = temporal_types_js_1.DateTime.fromStandardDate(new Date());
        temp_user.created = date;
        temp_user.updated = date;
        // Create Profile Object
        temp_profile.username = GenerateProfileUsername();
        temp_profile.uuid = uuid_1.v4();
        temp_profile.created = date;
        temp_profile.updated = date;
        let session = neo4j_1.default.session();
        try {
            yield session.run('CREATE (u:User $user_props)<-[:BELONGS_TO]-(p:Profile $profile_props)', { user_props: temp_user, profile_props: temp_profile });
        }
        catch (error) {
            logger_1.default.error('Database', error.message);
            dbr.status = false;
            dbr.message = error.message;
        }
        finally {
            session.close();
        }
        return dbr;
    });
}
exports.CreateUser = CreateUser;
function Authenticate(credentials) {
    return __awaiter(this, void 0, void 0, function* () {
        var returnData = { status: false };
        let session = neo4j_1.default.session();
        let results = yield session.run('MATCH ( u:User { email: $email } ) RETURN u', { email: credentials.email });
        if (results.records.length == 0) {
            returnData.message = `Email ${credentials.email} not found`;
            returnData.status = false;
        }
        else {
            let user = util_1.process_query_single_password(results.records[0]);
            if (bcrypt_1.default.compareSync(credentials.password, user.password)) {
                returnData.message = `User ${credentials.email} successfully authenticated`;
                returnData.status = true;
                returnData.user = lodash_1.default.omit(user, 'password');
            }
            else {
                returnData.message = `Password is invalid`;
                returnData.status = false;
            }
        }
        return returnData;
    });
}
exports.Authenticate = Authenticate;
function ChangePassword(credentials) {
    return __awaiter(this, void 0, void 0, function* () {
        var dbr = {
            status: true,
            message: '',
            data: null
        };
        let session = neo4j_1.default.session();
        credentials.password = bcrypt_1.default.hashSync(credentials.password, saltRounds);
        let results = yield session.run('MATCH ( u:User { email: $email } ) SET u.password = $password RETURN u', { email: credentials.email, password: credentials.password });
        if (results.records.length != 0) {
            dbr.data = util_1.process_query_single(results.records[0]);
            return dbr;
        }
        else {
            return dbr;
        }
    });
}
exports.ChangePassword = ChangePassword;
