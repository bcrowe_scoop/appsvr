"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = exports.publish = void 0;
var amqp = require('amqplib/callback_api');
const config_1 = __importDefault(require("./config"));
const logger_1 = __importDefault(require("./logger"));
const ns = 'RabbitMQ';
// if the connection is closed or fails to be established at all, we will reconnect
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];
function closeOnErr(err) {
    if (!err)
        return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}
function publish(exchange, routingKey, data) {
    logger_1.default.info(ns, `[AMQP] Publishing job to Exchange: ${exchange}, Worker queue: ${routingKey}`);
    // Parse data into Buffer from JSON string
    try {
        var content = Buffer.from(JSON.stringify(data));
    }
    catch (error) {
        logger_1.default.error(ns, `Publish error: ${error.message}`);
    }
    try {
        pubChannel.publish(exchange, routingKey, content, { persistent: true }, function (err, ok) {
            if (err) {
                logger_1.default.error(ns, `Publish error:, ${err.message}`);
                offlinePubQueue.push([exchange, routingKey, content]);
                pubChannel.connection.close();
            }
        });
    }
    catch (e) {
        console.error(ns, `[AMQP] publish error:, ${e.message}`);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}
exports.publish = publish;
function start() {
    return new Promise((resolve, reject) => {
        if (amqpConn) {
            resolve(amqpConn);
        }
        else {
            var url = `amqp://${config_1.default.rabbitmq.host}` + "?heartbeat=60";
            logger_1.default.info(ns, `Connecting to RabbitMQ at ${url}`);
            amqp.connect(url, function (err, conn) {
                if (err) {
                    logger_1.default.error(ns, `Connection error:, ${err.message}`);
                    return setTimeout(start, 1000);
                }
                conn.on("error", function (err) {
                    if (err.message !== "Connection closing") {
                        logger_1.default.error(ns, `Connection error", ${err.message}`);
                    }
                });
                conn.on("close", function () {
                    logger_1.default.error(ns, "Reconnecting");
                    return setTimeout(start, 1000);
                });
                logger_1.default.info(ns, "Connected");
                amqpConn = conn;
                // Start Publisher
                amqpConn.createConfirmChannel(function (err, ch) {
                    if (closeOnErr(err))
                        return;
                    ch.on("error", function (err) {
                        logger_1.default.error(ns, `Channel error:, ${err.message}`);
                    });
                    ch.on("close", function () {
                        logger_1.default.info(ns, `Channel closed`);
                    });
                    pubChannel = ch;
                    resolve(amqpConn);
                    while (true) {
                        var m = offlinePubQueue.shift();
                        if (!m)
                            break;
                        publish(m[0], m[1], m[2]);
                    }
                });
            });
        }
    });
}
exports.start = start;
