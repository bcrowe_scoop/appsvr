"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = __importDefault(require("events"));
const emitter = new events_1.default.EventEmitter();
const rabbitmq_1 = require("./rabbitmq");
const config_1 = __importDefault(require("./config"));
emitter.on('/video/uploaded', ({ user_uuid, video }) => {
    // Create object for worker queue
    var worker_data = {
        action: 'process_video',
        user_uuid: user_uuid,
        data: video,
        timestamp: new Date()
    };
    rabbitmq_1.publish('', config_1.default.rabbitmq.worker_queue, worker_data);
});
emitter.on('/video/deleted', ({ user_uuid, video }) => {
    // Create object for worker queue
    var worker_data = {
        action: 'delete_video',
        user_uuid: user_uuid,
        data: video,
        timestamp: new Date()
    };
    console.log(`[AMQP] Sending job action: ${worker_data.action} for user_id: ${worker_data.user_uuid} on video: ${worker_data.data.uuid}`);
    rabbitmq_1.publish('', config_1.default.rabbitmq.worker_queue, worker_data);
});
exports.default = emitter;
