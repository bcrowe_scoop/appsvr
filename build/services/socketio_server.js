"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_1 = __importDefault(require("socket.io"));
const socket_io_redis_1 = require("socket.io-redis");
const logger_1 = __importDefault(require("./logger"));
const config_1 = __importDefault(require("./config"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const ns = 'SocketIO';
var io;
function default_1(server) {
    return new Promise((resolve, reject) => {
        if (!io) {
            io = new socket_io_1.default.Server(server, {
                cors: {
                    origin: 'http://localhost:8080',
                    methods: ["GET", "POST"],
                    allowedHeaders: ["token"],
                    credentials: true
                }
            });
            // SocketIO Authentication by JWT token.
            io.use((socket, next) => {
                try {
                    socket.decoded_token = jsonwebtoken_1.default.verify(socket.handshake.headers.token, config_1.default.jwt_secret);
                }
                catch (err) {
                    console.error(`[SocketIO]: ${err.message}`);
                    return next(err);
                }
                return next();
            });
            // Attache Redis Adapter
            io.adapter(socket_io_redis_1.createAdapter(`redis://${config_1.default.redis.host}`));
            logger_1.default.info('[Redis]', `[Redis] SocketIO connected to Redis`);
            // On Connection, subscribe to a room with user's uuid
            io.on('connection', socket => {
                logger_1.default.info(ns, `New connection: email: ${socket.decoded_token.email}, uuid: ${socket.decoded_token.uuid}`);
                logger_1.default.info(ns, `Email: ${socket.decoded_token.email} Joining room uuid: ${socket.decoded_token.uuid}`);
                socket.join(socket.decoded_token.uuid);
            });
            resolve(io);
        }
        else {
            resolve(io);
        }
    });
}
exports.default = default_1;
