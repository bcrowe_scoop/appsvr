"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const neo4j_driver_1 = __importDefault(require("neo4j-driver"));
const config_1 = __importDefault(require("./config"));
const driver = neo4j_driver_1.default.driver(`neo4j://${config_1.default.database.host}:${config_1.default.database.port}`, null, { disableLosslessIntegers: true });
exports.default = driver;
