"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = void 0;
// This file hold the code to intialize the database
const neo4j_1 = __importDefault(require("../neo4j"));
const logger_1 = __importDefault(require("../logger"));
const auth_1 = require("../auth");
const session = neo4j_1.default.session();
const ns = 'Database';
const categories = [
    { name: 'Film & Animation' },
    { name: 'Entertainment' },
    { name: 'News & Politics' },
    { name: 'Howto & Style' },
    { name: 'Education' },
    { name: 'Science & Technology' },
    { name: 'Nonprofits & Activism' },
    { name: 'Religion' },
    { name: 'Food & Culinary' },
    { name: 'Books & Reading' },
    { name: 'Autos & Vehicles' },
    { name: 'Music' },
    { name: 'Pets & Animals' },
    { name: 'Sports' },
    { name: 'Hobby & Recreation' },
    { name: 'Travel & Events' },
    { name: 'Gaming' },
    { name: 'People & Blogs' },
    { name: 'Comedy' },
];
const constraints = [
    'CREATE CONSTRAINT unique_user_email IF NOT EXISTS ON(user:User) ASSERT user.email IS UNIQUE',
    'CREATE CONSTRAINT unique_user_phone IF NOT EXISTS ON(user:User) ASSERT user.phone IS UNIQUE',
    'CREATE CONSTRAINT unique_profile_username IF NOT EXISTS ON(p:Profile) ASSERT p.username IS UNIQUE',
    'CREATE CONSTRAINT unique_profile_uuid IF NOT EXISTS ON(p:Profile) ASSERT p.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_user_uuid IF NOT EXISTS ON(user:User) ASSERT user.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_video_uuid IF NOT EXISTS ON(video:Video) ASSERT video.uuid IS UNIQUE',
    'CREATE CONSTRAINT unique_category_name IF NOT EXISTS ON(category:Category) ASSERT category.name IS UNIQUE',
    'CREATE CONSTRAINT unique_hashtag_name IF NOT EXISTS ON(hashtag:Hashtag) ASSERT hashtag.name IS UNIQUE'
];
const category_query = `
UNWIND $categories AS category
MERGE (:Category {name: category.name})
`;
function createConstraints() {
    return __awaiter(this, void 0, void 0, function* () {
        logger_1.default.info(ns, 'Creating Database Constraints');
        for (const c of constraints) {
            yield session.run(c);
        }
    });
}
function createCategories() {
    return __awaiter(this, void 0, void 0, function* () {
        logger_1.default.info(ns, 'Creating Categories');
        yield session.run(category_query, { categories });
    });
}
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        yield createConstraints();
        yield createCategories();
        yield auth_1.CreateUser({ email: 'admin', password: 'password' });
        logger_1.default.info(ns, 'Database initialized');
    });
}
exports.init = init;
