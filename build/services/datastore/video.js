"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Video = void 0;
const util_1 = require("./util");
const temporal_types_js_1 = require("neo4j-driver/lib/temporal-types.js");
class Video {
    constructor(driver) {
        this.driver = driver;
        this.label = 'Video';
    }
    select() {
        return __awaiter(this, void 0, void 0, function* () {
            const ses = this.driver.session();
            try {
                const result = yield ses.run(`MATCH (objects:${this.label}) RETURN objects`);
                return util_1.process_query_array(result.records);
            }
            finally {
                yield ses.close();
            }
        });
    }
    insert(user_uuid, doc) {
        return __awaiter(this, void 0, void 0, function* () {
            const ses = this.driver.session();
            doc.created = temporal_types_js_1.DateTime.fromStandardDate(new Date());
            doc.updated = temporal_types_js_1.DateTime.fromStandardDate(new Date());
            try {
                var q = `
            MATCH (p:Profile)-[:BELONGS_TO]->(u:User {uuid: $user_uuid})
            CREATE (v:Video $video_props)-[:CREATED_BY]->(p)
            `;
                const result = yield ses.run(q, { video_props: doc, user_uuid });
                return util_1.process_query_array(result.records);
            }
            finally {
                yield ses.close();
            }
        });
    }
}
exports.Video = Video;
