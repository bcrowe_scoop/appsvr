"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.process_query_single_password = exports.process_query_single = exports.process_query_array = void 0;
const lodash_1 = __importDefault(require("lodash"));
function process_query_array(results) {
    return results.map(object => {
        let temp = object._fields[0].properties;
        temp.id = object._fields[0].identity;
        temp = lodash_1.default.omit(temp, 'password');
        return temp;
    });
}
exports.process_query_array = process_query_array;
function process_query_single(record) {
    let temp = record._fields[0].properties;
    temp.id = record._fields[0].identity;
    temp = lodash_1.default.omit(temp, 'password');
    return temp;
}
exports.process_query_single = process_query_single;
function process_query_single_password(record) {
    let temp = record._fields[0].properties;
    temp.id = record._fields[0].identity;
    return temp;
}
exports.process_query_single_password = process_query_single_password;
