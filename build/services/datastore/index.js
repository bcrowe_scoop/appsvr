"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const query_class_1 = __importDefault(require("./query_class"));
const neo4j_1 = __importDefault(require("../neo4j"));
exports.default = {
    user: new query_class_1.default(neo4j_1.default, 'User')
};
