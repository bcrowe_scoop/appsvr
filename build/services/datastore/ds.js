"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ds = void 0;
const { v4: uuidv4 } = require('uuid');
const temporal_types_js_1 = require("neo4j-driver/lib/temporal-types.js");
const _ = require('lodash');
function map_records(records) {
    return records.map(r => {
        return r._fields[0];
    });
}
function map_record(records) {
    if (records.length > 0) {
        return records[0]._fields[0];
    }
    return {};
}
class ds {
    constructor(driver, label) {
        this.driver = driver;
        this.label = label;
    }
    select() {
        return __awaiter(this, void 0, void 0, function* () {
            const session = this.driver.session();
            const query = `
                MATCH (u:${this.label} )RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ], playlists:[ (u)--(p:TestPLaylist) | p {.*} ]}
            `;
            try {
                var results = yield session.run(query);
                return map_records(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    selectOne(uuid) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = this.driver.session();
            const params = { uuid };
            const query = `
            MATCH (u:${this.label} {uuid: $uuid})RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ]}
            `;
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    insert(doc) {
        return __awaiter(this, void 0, void 0, function* () {
            doc.uuid = uuidv4();
            var current_date = new Date();
            doc.created = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            doc.updated = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            const session = this.driver.session();
            const params = { doc };
            const query = `
                CREATE (u:${this.label} $doc) RETURN u{.*}
            `;
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    insertWithRelationship(doc, relationship) {
        return __awaiter(this, void 0, void 0, function* () {
            // relationship reference
            // var relationship = {
            //     label: 'Label of the node to create relationship to',
            //     name: 'Name of relationship. This is the name of the relationship',
            //     uuid: 'uuid of the node to create relationship',
            //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
            // }
            doc.uuid = uuidv4();
            var current_date = new Date();
            doc.created = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            doc.updated = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            const session = this.driver.session();
            const params = { doc, uuid: relationship.uuid };
            var query = '';
            if (relationship.outbound) {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${this.label} $doc)
                    CREATE (u)-[r:${relationship.name}]->(n)
                    RETURN u{.*}
                `;
            }
            else {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${this.label} $doc)
                    CREATE (u)<-[r:${relationship.name}]-(n)
                    RETURN u{.*}
                `;
            }
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    mergeWithRelationship(doc, relationship) {
        return __awaiter(this, void 0, void 0, function* () {
            // relationship reference
            // var relationship = {
            //     label: 'Label of the node to create relationship to',
            //     name: 'Name of relationship. This is the name of the relationship',
            //     uuid: 'uuid of the node to create relationship',
            //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
            // }
            doc.uuid = uuidv4();
            var current_date = new Date();
            doc.created = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            doc.updated = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            const session = this.driver.session();
            const params = { doc, uuid: relationship.uuid };
            var query = '';
            if (relationship.outbound) {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    MERGE (u:${this.label} $doc)
                    CREATE (u)-[r:${relationship.name}]->(n)
                    RETURN u{.*}
                `;
            }
            else {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    MERGE (u:${this.label} $doc)
                    CREATE (u)<-[r:${relationship.name}]-(n)
                    RETURN u{.*}
                `;
            }
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    customquery(query, params) {
        return __awaiter(this, void 0, void 0, function* () {
            // relationship reference
            // var relationship = {
            //     label: 'Label of the node to create relationship to',
            //     name: 'Name of relationship. This is the name of the relationship',
            //     uuid: 'uuid of the node to create relationship',
            //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
            // }
            const session = this.driver.session();
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    update(uuid, doc) {
        return __awaiter(this, void 0, void 0, function* () {
            doc = _.omit(doc, 'uuid');
            var current_date = new Date();
            doc.updated = temporal_types_js_1.DateTime.fromStandardDate(current_date);
            const session = this.driver.session();
            const params = { doc, uuid };
            const query = `
                MATCH (p:${this.label} { uuid: $uuid })
                SET p += $doc
                RETURN p {.*}
            `;
            try {
                var results = yield session.run(query, params);
                return map_record(results.records);
            }
            finally {
                session.close();
            }
        });
    }
    delete(uuid) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = this.driver.session();
            const params = { uuid };
            const query = `
                MATCH (p:${this.label} { uuid: $uuid })
                DELETE p
            `;
            try {
                var results = yield session.run(query, params);
                return results;
            }
            finally {
                session.close();
            }
        });
    }
}
exports.ds = ds;
