"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.minioClient = void 0;
const config_1 = __importDefault(require("./config"));
const minio_1 = require("minio");
exports.minioClient = new minio_1.Client({
    endPoint: config_1.default.minio.endPoint,
    port: config_1.default.minio.port,
    useSSL: config_1.default.minio.useSSL,
    accessKey: config_1.default.minio.accessKey,
    secretKey: config_1.default.minio.secretKey
});
