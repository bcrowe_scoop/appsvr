FROM node:12.20.1

WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
RUN npm install
RUN npm run build
COPY /config ./build/config
EXPOSE 3000
CMD ["node", "./build/index.js"]
